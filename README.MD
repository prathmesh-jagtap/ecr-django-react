# Deploying services with AWS ECS

## Description

Terraform modules to work with AWS ECS. You will find reusable code in the folder modules and an example in the folder "create_cluster_with_services".
