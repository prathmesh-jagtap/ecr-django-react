module "vpc" {
    # source = "../modules/vpc_with_alb"
    source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-vpc-with-alb"
    namespace = var.namespace
    vpc_name = var.vpc_name
    vpc_cidr_block = var.vpc_cidr_block
    availability_zones = var.availability_zones
    application_balancer_name = var.application_balancer_name
    aws_security_group_load_balancer_id = aws_security_group.load-balancer.id
}

module "iam_roles_for_ecs_service" {
    source = "../modules/iam_roles_for_ecs_service"
    # source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/iam-roles-for-ecs-service"
    iam_role_name = "ecsInstanceRole"
}

module "ecr_with_applications" {
    # source = "../modules/ecr_with_images"    
    source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/ecr_with_images"
    namespace = var.namespace    
    stage = var.stage
    name = var.name
    region = var.region
    image_names = var.image_names
    docker_files = var.docker_files
}


module "task_definition_backend" {
    source = "../modules/task_definitions_for_ecs"    
    # source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/task_definitions_for_ecs"
    application  = var.backend_app
    image_url = module.ecr_with_applications.repository_url_map
    region = var.region
    taskexecutionrole  = "django-task-execution-role"
    depends_on = [
      module.ecr_with_applications
    ]
}

module "task_definition_frontend" {
    source = "../modules/task_definitions_for_ecs"    
    # source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/task_definitions_for_ecs"
    application  = var.frontend_app
    image_url = module.ecr_with_applications.repository_url_map
    region = var.region
    taskexecutionrole  = "react-task-execution-role"
    depends_on = [
      module.ecr_with_applications
    ]
}


module "auto_scaling_with_launch_template" {
    # source = "../modules/auto_scaling_with_launch_template"
    source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/auto_scaling_with_launch_template"
    vpc_id = module.vpc.vpc_id

    image_id = var.image_id
    instance_type = var.instance_type
    autoscaling_group_name = var.autoscaling_group_name
    capacity_provider_name = var.capacity_provider_name

    public_subnet_ids =  module.vpc.public_subnet_ids
    ecs_cluster_name = var.ecs_cluster_name
    aws_security_group_ecs_id = aws_security_group.ecs.id
    aws_iam_instance_profile_name = module.iam_roles_for_ecs_service.aws_iam_instance_profile_name

    min_size = var.min_size
    max_size = var.max_size
    desired_capacity = var.desired_capacity

    maximum_scaling_step_size= var.maximum_scaling_step_size
    minimum_scaling_step_size = var.minimum_scaling_step_size
    target_capacity = var.target_capacity


    depends_on = [
      module.vpc, module.iam_roles_for_ecs_service
    ]
}

module "aws_empty_ecs_cluster" {
    # source = "../modules/ecs_empty_cluster"
    source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/ecs_empty_cluster"
    cluster_name = var.ecs_cluster_name
    autoscaling_group_name =var.autoscaling_group_name
    capacity_provider_name = var.capacity_provider_name
    region = var.region
    depends_on = [
      module.vpc, module.iam_roles_for_ecs_service, module.auto_scaling_with_launch_template
    ]
}


module "certificate_manager" {
    source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/domain_with_ssl"
    hosted_zone_id = var.hosted_zone_id
    domain_name = var.domain_name
    aws_lb_zone_id = module.vpc.aws_lb_zone_id
    aws_lb_dns_name = module.vpc.aws_lb_dns_name
}

module "ecs_service_in_cluster_frontend" {
    # source = "../modules/ecs_service_in_cluster"
    source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/ecs_service_in_cluster"
    aws_ecs_cluster_id =  module.aws_empty_ecs_cluster.cluster_id
    vpc_id = module.vpc.vpc_id

    load_balancer_arn = module.vpc.aws_lb_arn
    ecs_iam_role_arn = module.iam_roles_for_ecs_service.aws_iam_role_arn
    acm_certificate_arn = module.certificate_manager.aws_acm_certificate_arn

    default_target_group_arn = ""
    create_aws_alb_listener_rule = false

    service = {
        name = var.service_names[1] # only alphanumeric characters and hyphens allowed
        container_name = var.frontend_app.container_name
        container_port = var.frontend_app.container_port
        host_port      = var.frontend_app.host_port
        protocol       = "HTTP"
        health_check_path = "/"
        desired_count = 1

        task_definition_arn      = module.task_definition_frontend.task_definition_arn
    }

    depends_on = [
      module.vpc, 
      module.certificate_manager, 
      module.iam_roles_for_ecs_service, 
      module.aws_empty_ecs_cluster      
    ]
}

module "ecs_service_in_cluster_backend" {
    # source = "../modules/ecs_service_in_cluster"
    source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/ecs_service_in_cluster"
    aws_ecs_cluster_id =  module.aws_empty_ecs_cluster.cluster_id
    vpc_id = module.vpc.vpc_id

    load_balancer_arn = module.vpc.aws_lb_arn
    ecs_iam_role_arn = module.iam_roles_for_ecs_service.aws_iam_role_arn
    acm_certificate_arn = module.certificate_manager.aws_acm_certificate_arn

    default_target_group_arn = module.ecs_service_in_cluster_frontend.aws_alb_target_group_arn
    create_aws_alb_listener_rule = true
    aws_alb_listener_rule_path = "/backend/*"

    service = {
        name = var.service_names[0] # only alphanumeric characters and hyphens allowed
        container_name = var.backend_app.container_name
        container_port = var.backend_app.container_port
        host_port      = var.backend_app.host_port
        protocol       = "HTTP"
        health_check_path = "/"
        desired_count = 1

        task_definition_arn      = module.task_definition_backend.task_definition_arn
    }

    depends_on = [
      module.vpc,
      module.ecs_service_in_cluster_frontend,
      module.certificate_manager, 
      module.iam_roles_for_ecs_service,
      module.aws_empty_ecs_cluster
    ]
}


module "cloudwatch" {
  source                        = "../modules/ecs_cloud_watch"
  cluster_arn                   = module.aws_empty_ecs_cluster.cluster_arn
  cluster_name                  = var.ecs_cluster_name
  cloudwatch_event_rule_name    = "ECS_Cluster_Rule"
  user_emails                   = ["phat.vo@tekos.net", "vtphat19@gmail.com"]
}
