output "application_load_balancer_arn" {
  value = aws_lb.default.arn
}

output "application_load_balancer_dns_name" {
  value = aws_lb.default.dns_name
}

output "application_load_balancer_zone_id" {
  value = aws_lb.default.zone_id
}

output "public_subnet_ids" {
  value = module.subnets.public_subnet_ids
  description = "The public subnet ids of VPC"
}

output "private_subnet_ids" {
  value = module.subnets.private_subnet_ids
  description = "The private subnet ids of VPC"
}

output "vpc_id" {
  value = module.vpc.vpc_id
   description = "ID of VPC"
}

output "vpc_cidr_block" {
  value       = module.vpc.vpc_cidr_block
  description = "VPC cidr block"
}

output "vpc_default_security_group_id" {
  value       = module.vpc.vpc_default_security_group_id
  description = "The ID of the security group created by default on VPC creation"
}

output "aws_lb_arn" {
  value = aws_lb.default.arn
}

output "aws_lb_zone_id" {
  value = aws_lb.default.zone_id
}

output "aws_lb_dns_name" {
  value = aws_lb.default.dns_name
}
