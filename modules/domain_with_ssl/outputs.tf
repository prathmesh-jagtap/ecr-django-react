output "aws_route53_record_name" {
  value = aws_route53_record.alias.name
}

output "aws_acm_certificate_arn" {
  value = aws_acm_certificate.cert.arn
}