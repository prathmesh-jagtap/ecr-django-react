variable "hosted_zone_id" {
  type = string
  description = "ID of hosted zone in AWS Route53"
}

variable "domain_name" {
  type = string
  description = "The domain name alias to the Application Load balancer"
}

variable "aws_lb_dns_name" {
  type = string
  description = "DNS name of AWS Load balancer"
}

variable "aws_lb_zone_id" {
  type = string
  description = "Hosted zone Id of AWS Load balancer"
}

