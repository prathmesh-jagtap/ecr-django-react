variable "cluster_name" {
  type = string
  description = "Cluster name"
}

variable "capacity_provider_name" {
  type = string
  description  = "Name of the Capacity provider"
  default = "default_capacity_provider"
}

variable "autoscaling_group_name" {
  type = string
  description = "Auto scaling group name"  
}

variable "region" {
  type        = string
  description = "AWS region"
}