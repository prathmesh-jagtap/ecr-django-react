locals {
  cmd = "#!/bin/bash\necho ECS_CLUSTER=${var.ecs_cluster_name} > /etc/ecs/ecs.config"
}

# !Image ID depends on region
resource "aws_launch_template" "ecs" {
  name_prefix   = "launch_template_ecs"
  image_id      = var.image_id
  instance_type = var.instance_type

  network_interfaces {
    associate_public_ip_address = true
    security_groups             = [var.aws_security_group_ecs_id]
  }

  iam_instance_profile {
    name = var.aws_iam_instance_profile_name
  }
  user_data = base64encode(local.cmd)
}

resource "aws_autoscaling_group" "default" {
  name             = var.autoscaling_group_name
  min_size         = var.min_size
  max_size         = var.max_size
  desired_capacity = var.desired_capacity
  launch_template {
    id      = aws_launch_template.ecs.id
    version = "$Latest"
  }
  vpc_zone_identifier = var.public_subnet_ids
  tag {
    key                 = "AmazonECSManaged"
    value               = ""
    propagate_at_launch = true
  }
  protect_from_scale_in = true
  depends_on = [aws_launch_template.ecs]

  timeouts {
    delete = "3m"
  }

  provisioner "local-exec" {
    when    = destroy
    working_dir = path.module
    command = <<-EOT
    python script/turn_off_protection.py "autoscaling_group_name=${self.name},availability_zone=${tolist(self.availability_zones)[0]}"
    EOT
  }


}

resource "aws_ecs_capacity_provider" "default" {
  name = var.capacity_provider_name

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.default.arn
    managed_termination_protection = "ENABLED"

    managed_scaling {
      maximum_scaling_step_size = var.maximum_scaling_step_size
      minimum_scaling_step_size = var.minimum_scaling_step_size
      status                    = "ENABLED"
      target_capacity           = var.target_capacity
    }
  }
}

